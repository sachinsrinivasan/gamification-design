import cv2
import numpy as np


class WritingInAir:
    def __init__(self):
        self.x1 = list()
        self.y1 = list()
        self.kernel = np.ones((5, 5), np.uint8)

    def drawing(self):
        cap = cv2.VideoCapture(0)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
        _, frame = cap.read()
        print(frame.shape)
        while True:
            ret, frame = cap.read()
            frame = cv2.flip(frame, +1)
            if not ret:
                break
            frame2 = cv2.cvtColor(frame, cv2.COLOR_BGR2HLS)
            if cv2.waitKey(1) == ord('s'):
                break

            lb = np.array([0, 0, 81])
            ub = np.array([185, 255, 255])

            mask = cv2.inRange(frame2, lb, ub)
            cv2.imshow('Mask', mask)

            res = cv2.bitwise_and(frame, frame, mask=mask)
            cv2.imshow('Res', res)
            #
            opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, self.kernel)
            cv2.imshow('Erosion', opening)

            dilation = cv2.dilate(opening, self.kernel, iterations=5)
            cv2.imshow('Dilation', dilation)

            contours, hierarchy = cv2.findContours(opening, cv2.RETR_TREE,
                                                   cv2.CHAIN_APPROX_SIMPLE)
            for contour in contours:
                if cv2.contourArea(contour) > 1500:
                    cnt = contour
            M = cv2.moments(cnt)
            cx = int(M['m10'] / M['m00'])
            cy = int(M['m01'] / M['m00'])
            cv2.circle(frame, (cx, cy), 5, [50, 120, 255], -1)
            extTop = tuple(cnt[cnt[:, :, 1].argmin()][0])
            print(cx - extTop[0])
            if abs(cy - extTop[1]) > 200 and abs(cx - extTop[0]) < 150:
                self.x1.append(extTop[0])
                self.y1.append(extTop[1])

            for i in range(len(self.x1)):
                cv2.circle(frame, (self.x1[i], self.y1[i]), 4, (255, 155, 100), 5)
            cv2.imshow('', frame)

        cap.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    wr = WritingInAir()
    wr.drawing()
